from django.test import LiveServerTestCase, TestCase, Client
from django.urls import resolve
from .views import landingpage
from .forms import StatusForm
from .models import StatusModel
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class UnitTest(TestCase):
    def test_apakah_url_bisa_diakses (self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)

    def test_apakah_ada_tulisan_halo_apa_kabar(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("Halo! Apa Kabar?", content)

    def test_url_memanggil_landingpageviews (self):
        found = resolve('/')
        self.assertEqual(found.func, landingpage)

    def test_views_memanggil_html_landingpage(self):
        response = Client().get('//')
        self.assertTemplateUsed(response, 'index.html')

    def test_apakah_punya_box_form(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("<form", content)

    def test_apakah_ada_button_submit(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("<button", content) 
        self.assertIn("Submit", content) 

    def test_ada_table_isi_status(self):
        response = Client().get('//')
        content = response.content.decode('utf8')
        self.assertIn("<table", content) 
        self.assertIn("Time", content) 
        self.assertIn("Status", content) 

    def test_apakah_teks_box_kosong_diterima_sebagai_status(self):
        yang_mau_dites = StatusForm(data= {'Status': ''})
        self.assertFalse(yang_mau_dites.is_valid())
        self.assertEqual(yang_mau_dites.errors['Status'], ["This field is required."])

    def test_apakah_model_bisa_bikin_status_atau_tidak(self):
        status_baru = StatusModel.objects.create(Status = 'Apa yang sedang kamu pikirkan?')
        hitung_status = StatusModel.objects.all().count()
        self.assertEqual(hitung_status, 1)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        # find the form element
        Status = selenium.find_element_by_id('id_Status')
        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        Status.send_keys('Coba Coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)



