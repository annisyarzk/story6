from django.db import models
from datetime import datetime
from django.core.exceptions import ValidationError

# Create your models here.
class StatusModel(models.Model):
	Status = models.CharField(max_length=300)
	Time = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.Status