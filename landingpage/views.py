from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import StatusModel


# Create your views here.
def landingpage(request):
	statusModel = StatusModel.objects.all()
	statusForm = StatusForm(request.POST or None) # request.POST buat validasi

	# Masukin dari form ke database
	if request.method == 'POST':
		if statusForm.is_valid():	# Kalau input valid
			statusForm.save()
			return redirect('/')

	args = {
		'statusmodel' : statusModel,
		'statusform' : statusForm,
	}
	return render(request, 'index.html', args)