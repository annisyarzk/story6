from django import forms
from .models import StatusModel
from django.forms import ModelForm, TextInput

class StatusForm(forms.ModelForm):
	class Meta:
		model = StatusModel
		fields = [
			'Status',		]
		widgets = {
			'Status': forms.TextInput(attrs={
				'class': 'form-control',
				'placeholder' : 'im so busy, im so happy right now'}),
		}